package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.MyBidsModel;
import com.vinbytes.binary.model.PurchaseBidsModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyBidsRepository extends MongoRepository<MyBidsModel, String> {

}
