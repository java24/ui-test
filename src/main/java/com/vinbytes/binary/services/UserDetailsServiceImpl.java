package com.vinbytes.binary.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String PhoneNoOrEmail) throws UsernameNotFoundException {
		UserModel userTemp = userRepository.findByEmail(PhoneNoOrEmail);
		if(userTemp==null){
			UserModel user = userRepository.findByPhoneNoOrEmail(PhoneNoOrEmail, PhoneNoOrEmail).orElseThrow(
					() -> new UsernameNotFoundException("User not found with phoneNo or email : " + PhoneNoOrEmail));

			return UserDetailsImpl.build(user);
		}
		return UserDetailsImpl.build(userTemp);
	}

	@Transactional
	public UserDetails loadUserById(String id) {
		UserModel user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));

		return UserDetailsImpl.build(user);
	}
}
