package com.vinbytes.binary.services;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

@Component
public class Scheduler {

    private final SimpMessagingTemplate simpMessagingTemplate;



    @Autowired
    private GreetingServices greetingServicesService;

    Scheduler(SimpMessagingTemplate simpMessagingTemplate, GreetingServices greetingServices) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.greetingServicesService = greetingServices;
    }

    @Scheduled(fixedRateString = "1500", initialDelayString = "0")
    public void schedulingTask() {
        greetingServicesService.sendMessages();

    }


    @Scheduled(fixedRateString = "5200", initialDelayString = "0")
    public void nootification() {

        greetingServicesService.sendNotification();
    }


/*    @Scheduled(fixedRateString = "1000", initialDelayString = "0")
    public void BtcScheduler() {
        Double btcValue= getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/bitcoin?base=USD");
        simpMessagingTemplate.convertAndSend("/topic/btcValue",btcValue);
       }*/

    @Scheduled(fixedRateString = "1000", initialDelayString = "0")
    public void currentServerDateScheduler() {
        Date currentServerDate = new Date();
        simpMessagingTemplate.convertAndSend("/topic/currentTime",currentServerDate);
    }


    @Scheduled(fixedRateString = "10000", initialDelayString = "0")
    public void schedulingTaskDashboard() {
        greetingServicesService.getTop5LeaderBoard();
        greetingServicesService.GetTop3HighestBidders();
        greetingServicesService.GetTop3HighestWonBidders();
    }



    @Scheduled(fixedRateString = "4000", initialDelayString = "0")
    public void walletScheduler() {
        greetingServicesService.sendWallet();
    }







    Double getCurrentCoinbaseValue(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            Reader streamReader = null;
            boolean Error = false;
            int status = con.getResponseCode();
            if (status > 299) {
                streamReader = new InputStreamReader(con.getErrorStream());
                Error = true;
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }
            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            double CurrentPrice = 0.00;
            if (!Error){
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
                JSONObject data = (JSONObject) json.get("data");
                JSONObject prices = (JSONObject) data.get("prices");
                CurrentPrice = Double.parseDouble((String) prices.get("latest"));
            }
            return CurrentPrice;
        }catch (IOException | ParseException e){
            return 0.00;
        }
    }


}
