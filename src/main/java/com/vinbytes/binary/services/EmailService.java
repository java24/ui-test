package com.vinbytes.binary.services;

import com.vinbytes.binary.model.PlayBidModel;
import com.vinbytes.binary.model.PurchaseBidsModel;
import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Service
public class EmailService {

    @Value("${binaryOptions.app.mailsender}")
    private String mailsender;

    private final JavaMailSender javaMailSender;


    private final TemplateEngine templateEngine;

    @Autowired
    private AmazonSimpleMailService amazonSimpleMailService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AmazonSimpleNotificationService amazonSimpleNotificationService;

    public EmailService(TemplateEngine templateEngine, JavaMailSender javaMailSender) {
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendMail(String username,String toEmail, String subject, String message) throws Exception {
        if(subject.equals("Verification Mail")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("verificationMail", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
        }else if( subject.equals("Welcome To Play-Crypto")==true) {
            Context context = new Context();
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("welcomeMail", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);

        }else if( subject.equals("Lost")==true) {
            Context context = new Context();
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("lost", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);

        }else if( subject.equals("Password Reset Request")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("forgetPassword", context);
//            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }else if( subject.equals("Password Reset Success")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("resetSuccess", context);
//            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }else if( subject.equals("Initiate Reset Password")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("resetPasswordInitiate", context);
//            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }else if( subject.equals("Purchase Success")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("PurchaseSuccess", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }else if( subject.equals("Notification Request")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("welcomeNotificationMail", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }

        else if( subject.equals("Notification Request")==true) {
            Context context = new Context();
            context.setVariable("message", message);
            context.setVariable("username", username);
            context.setVariable("email", toEmail);
            String TemplatedMessage = templateEngine.process("welcomeNotificationMail", context);
            amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);
            /*javax.mail.internet.MimeMessage mimeMessage =
                    javaMailSender.createMimeMessage(); MimeMessageHelper helper = new
                    MimeMessageHelper(mimeMessage); helper.setSubject(subject);
            helper.setText(process,true);
            helper.setTo(toEmail);
            helper.setFrom(mailsender);
            javaMailSender.send(mimeMessage);*/
        }
    }

    @Async
    public void sendSupportMail(String name, String email, long phone, String description, String subject, String type) throws Exception {
        Context context = new Context();
        context.setVariable("name", name);
        context.setVariable("email", email);
        context.setVariable("phone", phone);
        context.setVariable("description", description);
        context.setVariable("subject", subject);
        if(type.equals("contact")==true) {
            String TemplatedMessage = templateEngine.process("contact", context);
            amazonSimpleMailService.sendMail("tech-dev1@play-crypto.com", "you have a new enquiry!", TemplatedMessage);
        }else if( type.equals("helpdesk")==true) {
            String TemplatedMessage = templateEngine.process("helpdesk", context);
            amazonSimpleMailService.sendMail("tech-dev1@play-crypto.com", subject, TemplatedMessage);
        }
    }
    public void sendPurchaseMail(String username,String toEmail, String subject, String stock, String stockImage,
        String count, String BidAmount, String transactionAmount, Double totalAmount, LocalDateTime BiddingDate) throws Exception {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.US);
        String formatDateTime = BiddingDate.format(formatter);
        Context context = new Context();
        context.setVariable("username", username);
        context.setVariable("toEmail", toEmail);
        context.setVariable("subject", subject);
        context.setVariable("stock", stock);
        context.setVariable("stockImage", stockImage);
        context.setVariable("count", count);
        context.setVariable("BidAmount", BidAmount);
        context.setVariable("transactionAmount", transactionAmount);
        context.setVariable("totalAmount", totalAmount);
        context.setVariable("BiddingDate", formatDateTime);
        String TemplatedMessage = templateEngine.process("PurchaseSuccess", context);
        amazonSimpleMailService.sendMail(toEmail, subject, TemplatedMessage);

    }



    public void sendResultMail(PurchaseBidsModel oneBidPurchasedByUserInThisSameStock,PlayBidModel playBidModel) {

        try
        {
            if(oneBidPurchasedByUserInThisSameStock.getStatus().equals("Won"))
            {
                UserModel userModel= userRepository.findById(oneBidPurchasedByUserInThisSameStock.getUserId()).orElse(null);
                if (userModel!=null)
                {
                    Context context = new Context();
                    context.setVariable("username", userModel.getUsername());
                    context.setVariable("toEmail", userModel.getEmail());
                    context.setVariable("subject", "Won");
                    context.setVariable("stock", "Bitcoin");
                    context.setVariable("stockID", oneBidPurchasedByUserInThisSameStock.getStockId());
                    context.setVariable("BidType", playBidModel.getBidType());
                    context.setVariable("count", oneBidPurchasedByUserInThisSameStock.getNoOfBids());
                    context.setVariable("Amount", oneBidPurchasedByUserInThisSameStock.getCreditedUnitBtc());
                    String TemplatedMessage = templateEngine.process("winningMail", context);
                    amazonSimpleMailService.sendMail(userModel.getEmail(), "Won", TemplatedMessage);
                }
        }else if(oneBidPurchasedByUserInThisSameStock.getStatus().equals("Lost"))
            {
                UserModel userModel=userRepository.findById(oneBidPurchasedByUserInThisSameStock.getUserId()).orElse(null);
                if (userModel!=null)
                {
                    Context context = new Context();
                    context.setVariable("username", userModel.getUsername());
                    context.setVariable("toEmail", userModel.getEmail());
                    context.setVariable("subject", "Lost");
                    context.setVariable("stock", "Bitcoin");
                    context.setVariable("stockID", oneBidPurchasedByUserInThisSameStock.getStockId());
                    context.setVariable("BidType", playBidModel.getBidType());
                    context.setVariable("count", oneBidPurchasedByUserInThisSameStock.getNoOfBids());
                    context.setVariable("Amount", oneBidPurchasedByUserInThisSameStock.getCreditedUnitBtc());
                    String TemplatedMessage = templateEngine.process("lost", context);
                    amazonSimpleMailService.sendMail(userModel.getEmail(), "Lost", TemplatedMessage);
                }
            }


        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }



}