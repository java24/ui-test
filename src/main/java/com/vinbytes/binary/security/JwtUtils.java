package com.vinbytes.binary.security;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.vinbytes.binary.services.UserDetailsImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import io.jsonwebtoken.*;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${binaryOptions.app.jwtSecret}")
	private String jwtSecret;

	@Value("${binaryOptions.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	@Autowired
	UserRepository userRepository;

	public String generateJwtToken(Authentication authentication) {
		try {
			UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

			return Jwts.builder().setSubject((userPrincipal.getEmail())).setIssuedAt(new Date())
					.setIssuer("com.binaryOptions.in")
					.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
					.signWith(SignatureAlgorithm.HS256, jwtSecret).compact();

		} catch (Exception e) {
			UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
			return Jwts.builder().setSubject((userPrincipal.getEmail())).setIssuedAt(new Date())
					.setIssuer("com.binaryOptions.in")
					.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
					.signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
		}

	}

	public String generateJwtTokenGoogle(String googleUserMailId) {
		try {
			UserModel userModel = userRepository.findByEmail(googleUserMailId);

			return Jwts.builder().setSubject((userModel.getEmail())).setIssuedAt(new Date())
					.setIssuer("com.binaryOptions.in")
					.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
					.signWith(SignatureAlgorithm.HS256, jwtSecret).compact();

		} catch (Exception e) {
			UserModel userModel = userRepository.findByEmail(googleUserMailId);

			return Jwts.builder().setSubject((userModel.getEmail())).setIssuedAt(new Date())
					.setIssuer("com.binaryOptions.in")
					.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
					.signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
		}

	}

	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}

//	public String getJWTToken(String username) {
//		String token = Jwts
//				.builder()
//				.setIssuedAt(new Date())
//				.setIssuer("com.binaryOptions.in")
//				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
//				.signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
//
//		return token;
//	}
}
