package com.vinbytes.binary.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "User Model Details")

@Document(collection = "users")
public class UserModel{
	@Id
	@ApiModelProperty(value = "PK of User Model", required = true)
	private String id;

	private String refId;


	private String twilioId;

	//@NotBlank
	//@Size(min = 3, max = 20)
	@ApiModelProperty(value = "User Name of User Model", required = true)
	private String username;

	//@NotBlank
	//@Size(min = 3, max = 50)
	@Email
	@ApiModelProperty(value = "Email of User Model", required = true)
	private String email;


	private String cryptoAddress;

	//@NotBlank
	//@Size(min = 6, max = 50)
	@ApiModelProperty(value = "Password of User Model", required = true)
	private String password;

	//	@DBRef
//	@JsonIgnore
	@ApiModelProperty(value = "Role of User Model", required = true)
	private String roles;

	@ApiModelProperty(value = "Country Code of User Model", required = true)
	private String countryCode;

	@ApiModelProperty(value = "phoneNo of User Model", required = true)
	private String phoneNo;

	@ApiModelProperty(value = "profileUrl of User Model", required = true)
	private String profileUrl;

	@ApiModelProperty(value = "otp of User Model", required = true)
	private String otp;

	@ApiModelProperty(value = "verified of User Model", required = true)
	private boolean verified;

	@ApiModelProperty(value = "resetPassword of User Model", required = true)
	private boolean resetPassword;

	@ApiModelProperty(value = "Login Type of User Model", required = true)
	private String loginType;

	private List<String> typesOfLogins;

	@ApiModelProperty(value = "googleUserId of User Model", required = true)
	private String googleUserId;

	@ApiModelProperty(value = "appleUserId of User Model", required = true)
	private String appleUserId;

	@ApiModelProperty(value = "deleted of User Model", required = true)
	private boolean deleted;

	@ApiModelProperty(value = "viewed Tour of User Model", required = true)
	private boolean viewedTour = false;

	@ApiModelProperty(value = "lastLoginDate of User Model", required = true)
	private Date lastLoginDate;

	@ApiModelProperty(value = "joinedOnDate of User Model", required = true)
	private Date joinedOnDate;

	@ApiModelProperty(value = "passwordResetCount of User Model", required = true)
	private long passwordResetCount = 0 ;

	@ApiModelProperty(value = "passwordUpdatedCount of User Model", required = true)
	private long passwordUpdatedCount= 0 ;

	@ApiModelProperty(value = "blockedCount of User Model", required = true)
	private long blockedCount= 0 ;

	@ApiModelProperty(value = "blocked user of User Model", required = true)
	private boolean blocked ;

	@ApiModelProperty(value = "suspiciousCount of User Model", required = true)
	private long suspiciousCount= 0 ;

	@ApiModelProperty(value = "suspiciousCount of User Model", required = true)
	private boolean loggedIn ;

	private String walletId;

	private boolean walletInitialization;

	private List<String> purchaseId;

	@ApiModelProperty(value = "twoFactor of User Model", required = true)
	private boolean twoFactorFlag=false;

	private boolean verifiedForPass=false;

	private long loggedCount= 0 ;

	public UserModel() {
	}


	public UserModel(String username, @Email String email, String password,String countryCode, String phoneNo,
					 String profileUrl, String loginType, boolean deleted) {

		this.password = password;
		this.username = username;
		this.email = email;
		this.phoneNo = phoneNo;
		this.countryCode = countryCode;
		this.profileUrl = profileUrl;
		this.loginType = loginType;
		this.deleted = deleted;
	}

	public UserModel(String id, String refId, String twilioId, String username, String email, String cryptoAddress, String password, String roles, String countryCode, String phoneNo, String profileUrl, String otp, boolean verified, boolean resetPassword, String loginType, String googleUserId, String appleUserId, boolean deleted, boolean viewedTour, Date lastLoginDate, Date joinedOnDate, long passwordResetCount, long passwordUpdatedCount, long blockedCount, boolean blocked, long suspiciousCount, boolean loggedIn, String walletId, boolean walletInitialization, List<String> purchaseId, boolean twoFactorFlag, boolean verifiedForPass, long loggedCount) {
		this.id = id;
		this.refId = refId;
		this.twilioId = twilioId;
		this.username = username;
		this.email = email;
		this.cryptoAddress = cryptoAddress;
		this.password = password;
		this.roles = roles;
		this.countryCode = countryCode;
		this.phoneNo = phoneNo;
		this.profileUrl = profileUrl;
		this.otp = otp;
		this.verified = verified;
		this.resetPassword = resetPassword;
		this.loginType = loginType;
		this.googleUserId = googleUserId;
		this.appleUserId = appleUserId;
		this.deleted = deleted;
		this.viewedTour = viewedTour;
		this.lastLoginDate = lastLoginDate;
		this.joinedOnDate = joinedOnDate;
		this.passwordResetCount = passwordResetCount;
		this.passwordUpdatedCount = passwordUpdatedCount;
		this.blockedCount = blockedCount;
		this.blocked = blocked;
		this.suspiciousCount = suspiciousCount;
		this.loggedIn = loggedIn;
		this.walletId = walletId;
		this.walletInitialization = walletInitialization;
		this.purchaseId = purchaseId;
		this.twoFactorFlag = twoFactorFlag;
		this.verifiedForPass = verifiedForPass;
		this.loggedCount = loggedCount;
	}

	public UserModel(String id, String username, @Email String email, String password, String roles, String countryCode, String phoneNo, String profileUrl, String otp, boolean verified, boolean resetPassword, String googleUserId, boolean deleted, List<String> purchaseId) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.roles = roles;
		this.phoneNo = phoneNo;
		this.countryCode = countryCode;
		this.profileUrl = profileUrl;
		this.otp = otp;
		this.verified = verified;
		this.resetPassword = resetPassword;
		this.googleUserId = googleUserId;
		this.deleted = deleted;
		this.purchaseId = purchaseId;
	}

	public UserModel(String loginType, String username, String email, String googleUserId, String roles) {
		this.loginType = loginType;
		this.username = username;
		this.email = email;
		this.googleUserId = googleUserId;
		this.roles = roles;
	}

	public long getLoggedCount() {
		return loggedCount;
	}

	public void setLoggedCount(long loggedCount) {
		this.loggedCount = loggedCount;
	}

	public boolean isVerifiedForPass() {
		return verifiedForPass;
	}

	public void setVerifiedForPass(boolean verifiedForPass) {
		this.verifiedForPass = verifiedForPass;
	}

	public boolean isTwoFactorFlag() {
		return twoFactorFlag;
	}

	public void setTwoFactorFlag(boolean twoFactorFlag) {
		this.twoFactorFlag = twoFactorFlag;
	}


	public String getTwilioId() {
		return twilioId;
	}

	public String getCryptoAddress() {
		return cryptoAddress;
	}

	public void setCryptoAddress(String cryptoAddress) {
		this.cryptoAddress = cryptoAddress;
	}

	public void setTwilioId(String twilioId) {
		this.twilioId = twilioId;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public boolean isViewedTour() {
		return viewedTour;
	}

	public List<String> getTypesOfLogins() {
		return typesOfLogins;
	}

	public void setTypesOfLogins(List<String> typesOfLogins) {
		this.typesOfLogins = typesOfLogins;
	}

	public void setViewedTour(boolean viewedTour) {
		this.viewedTour = viewedTour;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public long getPasswordResetCount() {
		return passwordResetCount;
	}

	public void setPasswordResetCount(long passwordResetCount) {
		this.passwordResetCount = passwordResetCount;
	}

	public long getPasswordUpdatedCount() {
		return passwordUpdatedCount;
	}

	public void setPasswordUpdatedCount(long passwordUpdatedCount) {
		this.passwordUpdatedCount = passwordUpdatedCount;
	}

	public Date getJoinedOnDate() {
		return joinedOnDate;
	}

	public void setJoinedOnDate(Date joinedOnDate) {
		this.joinedOnDate = joinedOnDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public List<String> getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(List<String> purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(boolean resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public long getBlockedCount() {
		return blockedCount;
	}

	public void setBlockedCount(long blockedCount) {
		this.blockedCount = blockedCount;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public long getSuspiciousCount() {
		return suspiciousCount;
	}

	public void setSuspiciousCount(long suspiciousCount) {
		this.suspiciousCount = suspiciousCount;
	}

	public String getAppleUserId() {
		return appleUserId;
	}

	public void setAppleUserId(String appleUserId) {
		this.appleUserId = appleUserId;
	}

	@Override
	public String toString() {
		return String.format("UserModel[id=%s, username='%s', email='%s']", id, username, email);
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public boolean isWalletInitialization() {
		return walletInitialization;
	}

	public void setWalletInitialization(boolean walletInitialization) {
		this.walletInitialization = walletInitialization;
	}
}
