package com.vinbytes.binary.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@ApiModel(description = "Google Users Model Details")

@Document(collection = "gusers")
public class OauthGLoginModel {

    @Id
    @ApiModelProperty(value = "PK of Google Users Model", required = true)
    private String id;

    @ApiModelProperty(value = "User name of Google Users Model", required = true)
    private String name;

    @ApiModelProperty(value = "Mail Id of Google Users Model", required = true)
    private String email;

    @ApiModelProperty(value = "Mail Id of Google Users Model", required = true)
    private Date loginTime;

    public OauthGLoginModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
}
