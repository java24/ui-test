package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.mapping.Document;

@ApiModel(description = "Over all Progress Modal")
@Document(collection = "OverallProgress")
public class OverallProgressModal {


    @ApiModelProperty(reference = "Mongo ID", required = true)
    private String id;

    @ApiModelProperty(reference = "Total No Of Bids", required = true)
    private Double totalNoOfBids = 0.00;

    @ApiModelProperty(reference = "Total No Of Opening Soon Bids", required = true)
    private Double totalNoOfOpeningSoonBids = 0.00;

    @ApiModelProperty(reference = "Total No Of In Progress Bids", required = true)
    private Double totalNoOfInProgressBids = 0.00;

    @ApiModelProperty(reference = "Total No Of Closed Bids", required = true)
    private Double totalNoOfClosedBids = 0.00;

    @ApiModelProperty(reference = "Total No Of Completed Bids", required = true)
    private Double totalNoOfCompletedBids = 0.00;

    @ApiModelProperty(reference = "Total No Of Locked Bids", required = true)
    private Double totalNoOfLockedBids = 0.00;

    @ApiModelProperty(reference = "Total Revenue Generated", required = true)
    private Double totalRevenueGenerated = 0.00;

    @ApiModelProperty(reference = "Total Amount Rewarded To Winners", required = true)
    private Double totalAmountRewardedToWinners = 0.00;

    @ApiModelProperty(reference = "Total Amount Collected For Bids", required = true)
    private Double totalAmountCollectedForBids = 0.00;

    @ApiModelProperty(reference = "Total Amount Of Transaction Fee For Bids Purchased", required = true)
    private Double totalAmountOfTransactionFeeForBidsPurchased = 0.00;

    public OverallProgressModal() {
    }

    public OverallProgressModal(String id, Double totalNoOfBids, Double totalNoOfOpeningSoonBids, Double totalNoOfInProgressBids, Double totalNoOfClosedBids, Double totalNoOfCompletedBids, Double totalNoOfLockedBids, Double totalRevenueGenerated, Double totalAmountRewardedToWinners, Double totalAmountCollectedForBids, Double totalAmountOfTransactionFeeForBidsPurchased) {
        this.id = id;
        this.totalNoOfBids = totalNoOfBids;
        this.totalNoOfOpeningSoonBids = totalNoOfOpeningSoonBids;
        this.totalNoOfInProgressBids = totalNoOfInProgressBids;
        this.totalNoOfClosedBids = totalNoOfClosedBids;
        this.totalNoOfCompletedBids = totalNoOfCompletedBids;
        this.totalNoOfLockedBids = totalNoOfLockedBids;
        this.totalRevenueGenerated = totalRevenueGenerated;
        this.totalAmountRewardedToWinners = totalAmountRewardedToWinners;
        this.totalAmountCollectedForBids = totalAmountCollectedForBids;
        this.totalAmountOfTransactionFeeForBidsPurchased = totalAmountOfTransactionFeeForBidsPurchased;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getTotalNoOfBids() {
        return totalNoOfBids;
    }

    public void setTotalNoOfBids(Double totalNoOfBids) {
        this.totalNoOfBids = totalNoOfBids;
    }

    public Double getTotalNoOfOpeningSoonBids() {
        return totalNoOfOpeningSoonBids;
    }

    public void setTotalNoOfOpeningSoonBids(Double totalNoOfOpeningSoonBids) {
        this.totalNoOfOpeningSoonBids = totalNoOfOpeningSoonBids;
    }

    public Double getTotalNoOfLockedBids() {
        return totalNoOfLockedBids;
    }

    public void setTotalNoOfLockedBids(Double totalNoOfLockedBids) {
        this.totalNoOfLockedBids = totalNoOfLockedBids;
    }

    public Double getTotalNoOfInProgressBids() {
        return totalNoOfInProgressBids;
    }

    public void setTotalNoOfInProgressBids(Double totalNoOfInProgressBids) {
        this.totalNoOfInProgressBids = totalNoOfInProgressBids;
    }

    public Double getTotalNoOfClosedBids() {
        return totalNoOfClosedBids;
    }

    public void setTotalNoOfClosedBids(Double totalNoOfClosedBids) {
        this.totalNoOfClosedBids = totalNoOfClosedBids;
    }

    public Double getTotalNoOfCompletedBids() {
        return totalNoOfCompletedBids;
    }

    public void setTotalNoOfCompletedBids(Double totalNoOfCompletedBids) {
        this.totalNoOfCompletedBids = totalNoOfCompletedBids;
    }

    public Double getTotalRevenueGenerated() {
        return totalRevenueGenerated;
    }

    public void setTotalRevenueGenerated(Double totalRevenueGenerated) {
        this.totalRevenueGenerated = totalRevenueGenerated;
    }

    public Double getTotalAmountRewardedToWinners() {
        return totalAmountRewardedToWinners;
    }

    public void setTotalAmountRewardedToWinners(Double totalAmountRewardedToWinners) {
        this.totalAmountRewardedToWinners = totalAmountRewardedToWinners;
    }

    public Double getTotalAmountCollectedForBids() {
        return totalAmountCollectedForBids;
    }

    public void setTotalAmountCollectedForBids(Double totalAmountCollectedForBids) {
        this.totalAmountCollectedForBids = totalAmountCollectedForBids;
    }

    public Double getTotalAmountOfTransactionFeeForBidsPurchased() {
        return totalAmountOfTransactionFeeForBidsPurchased;
    }

    public void setTotalAmountOfTransactionFeeForBidsPurchased(Double totalAmountOfTransactionFeeForBidsPurchased) {
        this.totalAmountOfTransactionFeeForBidsPurchased = totalAmountOfTransactionFeeForBidsPurchased;
    }
}
