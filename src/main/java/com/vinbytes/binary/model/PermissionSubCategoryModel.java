package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModelProperty;

public class PermissionSubCategoryModel {

    @ApiModelProperty(value = "name of SubCategory Model", required = true)
    public String name;
    @ApiModelProperty(value = "Attributes of SubCategory Model", required = true)
    public PermitNotificationModel attr;

    public PermissionSubCategoryModel() {
    }

    public PermissionSubCategoryModel(String name, PermitNotificationModel attr) {
        this.name = name;
        this.attr = attr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PermitNotificationModel getAttr() {
        return attr;
    }

    public void setAttr(PermitNotificationModel attr) {
        this.attr = attr;
    }
}
