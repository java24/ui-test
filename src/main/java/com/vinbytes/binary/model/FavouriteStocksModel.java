package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;

public class FavouriteStocksModel {

    @ApiModelProperty(value = "stockName", required = true)
    private String stockName;

    @ApiModelProperty(value = "symbol", required = true)
    private String symbol;

    @ApiModelProperty(value = "stockUrl", required = true)
    private String stockUrl;

    public FavouriteStocksModel() {
    }

    public FavouriteStocksModel(String stockName, String symbol, String stockUrl) {
        this.stockName = stockName;
        this.symbol = symbol;
        this.stockUrl = stockUrl;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStockUrl() {
        return stockUrl;
    }

    public void setStockUrl(String stockUrl) {
        this.stockUrl = stockUrl;
    }
}
