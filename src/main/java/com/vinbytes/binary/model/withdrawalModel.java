package com.vinbytes.binary.model;

public class withdrawalModel {

    private String userId;
    private String paymentMode;
    private String address;
    private Double amount;

    public withdrawalModel() {
    }

    public withdrawalModel(String userId, String paymentMode, String address, Double amount) {
        this.userId = userId;
        this.paymentMode = paymentMode;
        this.address = address;
        this.amount = amount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}