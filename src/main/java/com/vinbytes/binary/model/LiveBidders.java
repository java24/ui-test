package com.vinbytes.binary.model;

public class LiveBidders {


    private String userName;
    private String imageURl;
    private Double maxAmountInvested=0.00;


    public LiveBidders() {
    }

    public LiveBidders(String userName, String imageURl) {
        this.userName = userName;
        this.imageURl = imageURl;
    }

    public LiveBidders(String userName, String imageURl, Double maxAmountInvested) {
        this.userName = userName;
        this.imageURl = imageURl;
        this.maxAmountInvested = maxAmountInvested;
    }

    public Double getMaxAmountInvested() {
        return maxAmountInvested;
    }

    public void setMaxAmountInvested(Double maxAmountInvested) {
        this.maxAmountInvested = maxAmountInvested;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageURl() {
        return imageURl;
    }

    public void setImageURl(String imageURl) {
        this.imageURl = imageURl;
    }
}
