package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;

@ApiModel("Range Model")
public class RangeCountModel {

    @ApiModelProperty(value = "Range", required = true)
    private String range;

    @ApiModelProperty(value = "Up", required = true)
    private String up;

    @ApiModelProperty(value = "Down", required = true)
    private String down;

    public RangeCountModel() {
    }

    public RangeCountModel(String range, String up, String down) {
        this.range = range;
        this.up = up;
        this.down = down;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String getDown() {
        return down;
    }

    public void setDown(String down) {
        this.down = down;
    }
}
